package com.ksagaleev.timetrackingservice.controller;

import com.ksagaleev.timetrackingservice.model.converter.impl.AccountConverter;
import com.ksagaleev.timetrackingservice.model.converter.impl.ProjectConverter;
import com.ksagaleev.timetrackingservice.model.converter.impl.TimeTrackRecordConverter;
import com.ksagaleev.timetrackingservice.model.dto.AccountDTO;
import com.ksagaleev.timetrackingservice.model.dto.AddAccountDTO;
import com.ksagaleev.timetrackingservice.model.dto.ProjectDTO;
import com.ksagaleev.timetrackingservice.model.dto.TimeTrackRecordDTO;
import com.ksagaleev.timetrackingservice.model.entity.Account;
import com.ksagaleev.timetrackingservice.model.entity.Permission;
import com.ksagaleev.timetrackingservice.model.entity.Project;
import com.ksagaleev.timetrackingservice.model.entity.TimeTrackRecord;
import com.ksagaleev.timetrackingservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.ksagaleev.timetrackingservice.security.PermissionsConfig.CHANGE_ACCOUNTS;
import static com.ksagaleev.timetrackingservice.security.PermissionsConfig.TRACK_TIME;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountConverter accountDtoConverter;

    @Autowired
    private ProjectConverter projectConverter;

    @Autowired
    private TimeTrackRecordConverter timeTrackRecordConverter;

    @GetMapping
    public ResponseEntity<List<AccountDTO>> getAccounts() {
        List<Account> accounts = accountService.getAll();
        List<AccountDTO> accountDTOS = accountDtoConverter.convertAllToDTO(accounts);
        HttpStatus status = accountDTOS == null ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return new ResponseEntity<>(accountDTOS, status);
    }

    @GetMapping(value = "/{id}")
    @Secured(CHANGE_ACCOUNTS)
    public ResponseEntity<AccountDTO> getAccount(@PathVariable("id") Long id) {
        Account account = accountService.getAccount(id);
        return new ResponseEntity<>(accountDtoConverter.convertToDTO(account), HttpStatus.OK);
    }

    @PostMapping
    @Secured(CHANGE_ACCOUNTS)
    public ResponseEntity<AccountDTO> addAccount(@Valid @RequestBody AddAccountDTO addAccountDTO) {
        Account addedAccount = accountService.addAccount(addAccountDTO);
        HttpStatus status = addedAccount == null ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return new ResponseEntity<>(accountDtoConverter.convertToDTO(addedAccount), status);
    }

    @DeleteMapping(value = "/{id}")
    @Secured(CHANGE_ACCOUNTS)
    public ResponseEntity deleteAccount(@PathVariable("id") Long id) {
        accountService.deleteAccount(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    @Secured(CHANGE_ACCOUNTS)
    public ResponseEntity<AccountDTO> updateAccount(@PathVariable("id") Long id,
                                                    @Valid @RequestBody AccountDTO accountDtoToUpdate) {
        Account accountToUpdate = accountDtoConverter.convertToEntity(accountDtoToUpdate);
        Account updatedAccount = accountService.updateAccount(id, accountToUpdate);
        return new ResponseEntity<>(accountDtoConverter.convertToDTO(updatedAccount), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/time-track-records")
    @Secured(CHANGE_ACCOUNTS)
    public ResponseEntity<List<TimeTrackRecordDTO>> getAccountTimeTrackRecords(@PathVariable("id") Long id) {
        List<TimeTrackRecord> timeTrackRecords = accountService.getAccountTimeTrackRecords(id);
        return new ResponseEntity<>(timeTrackRecordConverter.convertAllToDTO(timeTrackRecords), HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/time-track-records")
    @Secured(CHANGE_ACCOUNTS)
    public ResponseEntity<List<TimeTrackRecordDTO>> addTimeTrackRecordToAccount(@PathVariable("id") Long id,
                                                                                @RequestBody TimeTrackRecordDTO timeTrackRecordDTO) {
        TimeTrackRecord timeTrackRecord = timeTrackRecordConverter.convertToEntity(timeTrackRecordDTO);
        List<TimeTrackRecord> timeTrackRecords = accountService.addTimeTrackRecordToAccount(id, timeTrackRecord);
        return new ResponseEntity<>(timeTrackRecordConverter.convertAllToDTO(timeTrackRecords), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/permissions")
    @Secured(CHANGE_ACCOUNTS)
    public ResponseEntity<Set<Permission>> getAccountPermissions(@PathVariable("id") Long id) {
        Set<Permission> authorities = accountService.getAccountPermissions(id);
        return new ResponseEntity<>(authorities, HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/permissions")
    @Secured(CHANGE_ACCOUNTS)
    public ResponseEntity<Set<Permission>> addPermissionToAccount(@PathVariable("id") Long id,
                                                                  @RequestBody Permission permission) {
        Set<Permission> authorities = accountService.addPermissionToAccount(id, permission);
        return new ResponseEntity<>(authorities, HttpStatus.OK);
    }

    @GetMapping(value = "/me")
    public ResponseEntity<AccountDTO> getMe(Authentication authentication) {
        Account myAccount = accountService.getMe(authentication);
        return new ResponseEntity<>(accountDtoConverter.convertToDTO(myAccount), HttpStatus.OK);
    }

    @GetMapping(value = "/me/projects")
    @Secured(TRACK_TIME)
    public ResponseEntity<List<ProjectDTO>> getAvailableProjects(Authentication authentication) {
        Set<Project> projects = accountService.getMyAvailableProjects(authentication);
        List<ProjectDTO> projectDTOS = projectConverter.convertAllToDTO(new ArrayList<>(projects));
        return new ResponseEntity<>(projectDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/me/time-track-records")
    public ResponseEntity<List<TimeTrackRecordDTO>> getMyTimeTrackRecords(Authentication authentication) {
        List<TimeTrackRecord> myTimeTrackRecords = accountService.getMyTimeTrackRecords(authentication);
        return new ResponseEntity<>(timeTrackRecordConverter.convertAllToDTO(myTimeTrackRecords), HttpStatus.OK);
    }

    @PostMapping(value = "/me/time-track-records")
    @Secured(TRACK_TIME)
    public ResponseEntity<List<TimeTrackRecordDTO>> trackMyTime(Authentication authentication,
                                                                @RequestBody TimeTrackRecordDTO timeTrackRecordDTO) {
        TimeTrackRecord timeTrackRecord = timeTrackRecordConverter.convertToEntity(timeTrackRecordDTO);
        List<TimeTrackRecord> timeTrackRecords = accountService.trackMyTime(authentication, timeTrackRecord);
        return new ResponseEntity<>(timeTrackRecordConverter.convertAllToDTO(timeTrackRecords), HttpStatus.OK);
    }

    @PutMapping(value = "/me/time-track-records/{id}")
    @Secured(TRACK_TIME)
    public ResponseEntity<TimeTrackRecordDTO> updateMyTimeTrackRecord(Authentication authentication,
                                                                   @PathVariable("id") Long recordId,
                                                                   @RequestBody TimeTrackRecordDTO recordDTO) {
        TimeTrackRecord recordToUpdate = timeTrackRecordConverter.convertToEntity(recordDTO);
        TimeTrackRecord updatedTimeTrackRecord = accountService.updateMyTimeTrackRecord(authentication, recordId, recordToUpdate);
        return new ResponseEntity<>(timeTrackRecordConverter.convertToDTO(updatedTimeTrackRecord), HttpStatus.OK);
    }
}
