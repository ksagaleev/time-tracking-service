package com.ksagaleev.timetrackingservice.controller;

import com.ksagaleev.timetrackingservice.model.converter.DtoConverter;
import com.ksagaleev.timetrackingservice.model.converter.impl.AccountConverter;
import com.ksagaleev.timetrackingservice.model.dto.AccountDTO;
import com.ksagaleev.timetrackingservice.model.dto.ProjectDTO;
import com.ksagaleev.timetrackingservice.model.entity.Account;
import com.ksagaleev.timetrackingservice.model.entity.Project;
import com.ksagaleev.timetrackingservice.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.ksagaleev.timetrackingservice.security.PermissionsConfig.CHANGE_ACCESS;
import static com.ksagaleev.timetrackingservice.security.PermissionsConfig.MANAGE_PROJECTS;

@RestController
@RequestMapping("/api/projects")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private DtoConverter<ProjectDTO, Project> projectDtoConverter;

    @Autowired
    private AccountConverter accountConverter;

    @GetMapping
    @Secured(MANAGE_PROJECTS)
    public ResponseEntity<List<ProjectDTO>> getAllProjects() {
        return new ResponseEntity<>(projectDtoConverter.convertAllToDTO(projectService.getAllProjects()), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    @Secured(MANAGE_PROJECTS)
    public ResponseEntity<ProjectDTO> getProject(@PathVariable("id") Long id) {
        Project project = projectService.getProject(id);
        HttpStatus status = project != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(projectDtoConverter.convertToDTO(project), status);
    }

    @PostMapping
    @Secured(MANAGE_PROJECTS)
    public ResponseEntity<ProjectDTO> addProject(@Valid @RequestBody ProjectDTO projectDtoToSave) {
        Project projectToSave = projectDtoConverter.convertToEntity(projectDtoToSave);
        Project savedProject = projectService.addProject(projectToSave);
        HttpStatus status = savedProject != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(projectDtoConverter.convertToDTO(savedProject), status);
    }

    @PutMapping(value = "/{id}")
    @Secured(MANAGE_PROJECTS)
    public ResponseEntity<ProjectDTO> updateProject(@PathVariable("id") Long id,
                                                    @Valid @RequestBody ProjectDTO projectDtoToUpdate) {
        Project projectToUpdate = projectDtoConverter.convertToEntity(projectDtoToUpdate);
        Project updatedProject = projectService.updateProject(id, projectToUpdate);
        return new ResponseEntity<>(projectDtoConverter.convertToDTO(updatedProject), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @Secured(MANAGE_PROJECTS)
    public ResponseEntity deleteProject(@PathVariable("id") Long id) {
        projectService.deleteProject(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/accounts")
    @Secured(CHANGE_ACCESS)
    public ResponseEntity<List<AccountDTO>> getAccounts(@PathVariable("id") Long projectId) {
        List<Account> accounts = projectService.getAccounts(projectId);
        List<AccountDTO> accountDTOS = accountConverter.convertAllToDTO(accounts);
        return new ResponseEntity<>(accountDTOS, HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/accounts")
    @Secured(CHANGE_ACCESS)
    public ResponseEntity<List<AccountDTO>> addAccountToProject(@PathVariable("id") Long projectId,
                                                                @RequestParam Long accountId) {
        List<Account> accounts = projectService.addAccountToProject(projectId, accountId);
        List<AccountDTO> accountDTOS = accountConverter.convertAllToDTO(accounts);
        return new ResponseEntity<>(accountDTOS, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}/accounts")
    @Secured(CHANGE_ACCESS)
    public ResponseEntity deleteAccountFromProject(@PathVariable("id") Long projectId,
                                                   @RequestParam Long accountId) {
        projectService.deleteAccountFromProject(projectId, accountId);
        return new ResponseEntity(HttpStatus.OK);
    }
}
