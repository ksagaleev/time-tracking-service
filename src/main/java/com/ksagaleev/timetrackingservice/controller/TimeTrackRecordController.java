package com.ksagaleev.timetrackingservice.controller;

import com.ksagaleev.timetrackingservice.model.converter.DtoConverter;
import com.ksagaleev.timetrackingservice.model.dto.TimeTrackRecordDTO;
import com.ksagaleev.timetrackingservice.model.entity.TimeTrackRecord;
import com.ksagaleev.timetrackingservice.service.TimeTrackRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

import static com.ksagaleev.timetrackingservice.security.PermissionsConfig.CHANGE_RECORDS;
import static com.ksagaleev.timetrackingservice.security.PermissionsConfig.LOCK_RECORDS;

@RestController
@RequestMapping("/api/time-track-records")
public class TimeTrackRecordController {

    @Autowired
    private TimeTrackRecordService timeTrackRecordService;

    @Autowired
    private DtoConverter<TimeTrackRecordDTO, TimeTrackRecord> timeTrackRecordDtoConverter;

    @GetMapping
    public List<TimeTrackRecordDTO> getAll() {
        return timeTrackRecordDtoConverter.convertAllToDTO(timeTrackRecordService.getAll());
    }

    @GetMapping(value = "/{id}")
    @Secured(CHANGE_RECORDS)
    public ResponseEntity<TimeTrackRecordDTO> getTimeTrackRecord(@PathVariable("id") Long id) {
        TimeTrackRecord timeTrackRecord = timeTrackRecordService.getTimeTrackRecord(id);
        HttpStatus status = timeTrackRecord != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(timeTrackRecordDtoConverter.convertToDTO(timeTrackRecord), status);
    }

    @PostMapping
    @Secured(CHANGE_RECORDS)
    public ResponseEntity<TimeTrackRecordDTO> addTimeTrackRecord(@Valid @RequestBody TimeTrackRecordDTO timeTrackRecordDtoToSave) {
        TimeTrackRecord timeTrackRecordToSave = timeTrackRecordDtoConverter.convertToEntity(timeTrackRecordDtoToSave);
        TimeTrackRecord savedTimeTrackRecord = timeTrackRecordService.addTimeTrackRecord(timeTrackRecordToSave);
        HttpStatus status = savedTimeTrackRecord != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(timeTrackRecordDtoConverter.convertToDTO(savedTimeTrackRecord), status);
    }

    @PutMapping(value = "/{id}")
    @Secured(CHANGE_RECORDS)
    public ResponseEntity<TimeTrackRecordDTO> updateTimeTrackRecord(@PathVariable("id") Long id,
                                                                    @Valid @RequestBody TimeTrackRecordDTO timeTrackRecordDTO) {
        TimeTrackRecord timeTrackRecordToUpdate = timeTrackRecordDtoConverter.convertToEntity(timeTrackRecordDTO);
        TimeTrackRecord updatedTimeTrackRecord = timeTrackRecordService.updateTimeTrackRecord(id, timeTrackRecordToUpdate);
        return new ResponseEntity<>(timeTrackRecordDtoConverter.convertToDTO(updatedTimeTrackRecord), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @Secured(CHANGE_RECORDS)
    public ResponseEntity deleteTimeTrackRecord(@PathVariable("id") Long id) {
        timeTrackRecordService.deleteTimeTrackRecord(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    @Secured(LOCK_RECORDS)
    public ResponseEntity finalizeTimeTrackRecords(
            @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate lockDate) {
        timeTrackRecordService.finalizeRecordsOlderThan(lockDate);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
