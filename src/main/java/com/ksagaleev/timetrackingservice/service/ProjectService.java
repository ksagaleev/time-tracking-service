package com.ksagaleev.timetrackingservice.service;

import com.ksagaleev.timetrackingservice.model.entity.Account;
import com.ksagaleev.timetrackingservice.model.entity.Project;

import java.util.List;

public interface ProjectService {
    List<Project> getAllProjects();
    Project getProject(Long id);
    Project addProject(Project projectToSave);
    Project updateProject(Long id, Project projectToUpdate);
    void deleteProject(Long id);
    List<Account> getAccounts(Long projectId);
    List<Account> addAccountToProject(Long projectId, Long accountId);
    void deleteAccountFromProject(Long projectId, Long accountId);
}
