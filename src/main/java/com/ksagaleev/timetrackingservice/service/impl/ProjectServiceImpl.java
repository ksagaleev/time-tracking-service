package com.ksagaleev.timetrackingservice.service.impl;

import com.ksagaleev.timetrackingservice.model.entity.Account;
import com.ksagaleev.timetrackingservice.model.entity.Project;
import com.ksagaleev.timetrackingservice.repo.AccountRepo;
import com.ksagaleev.timetrackingservice.repo.ProjectRepo;
import com.ksagaleev.timetrackingservice.service.ProjectService;
import com.ksagaleev.timetrackingservice.validation.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepo projectRepo;

    @Autowired
    private AccountRepo accountRepo;

    private final String NO_PROJECT_MESSAGE = "Project with this ID does not exist";
    private final String NO_ACCOUNT_MESSAGE = "User with this ID does not exist";

    @Override
    public List<Project> getAllProjects() {
        return projectRepo.findAll();
    }

    @Override
    public Project getProject(Long id) {
        return projectRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(NO_PROJECT_MESSAGE));
    }

    @Override
    public Project addProject(Project projectToSave) {
        return projectRepo.save(projectToSave);
    }

    @Override
    public Project updateProject(Long id, Project projectToUpdate) {
        return projectRepo.findById(id)
                .map(project -> {
                    project.setName(projectToUpdate.getName());
                    project.setParentProject(projectToUpdate.getParentProject());
                    return projectRepo.save(project);
                }).orElseThrow(() -> new ResourceNotFoundException(NO_PROJECT_MESSAGE));
    }

    @Override
    public void deleteProject(Long id) {
        Project project = projectRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(NO_PROJECT_MESSAGE));
        List<Project> childProjects = project.getChildProjects();
        childProjects.forEach(childProject -> childProject.setParentProject(project.getParentProject()));
        projectRepo.delete(project);
    }

    @Override
    public List<Account> getAccounts(Long projectId) {
        Project project = projectRepo.findById(projectId)
                .orElseThrow(() -> new ResourceNotFoundException(NO_PROJECT_MESSAGE));
        return new ArrayList<>(project.getAccounts());
    }

    @Override
    public List<Account> addAccountToProject(Long projectId, Long accountId) {
        Project project = projectRepo.findById(projectId)
                .orElseThrow(() -> new ResourceNotFoundException(NO_PROJECT_MESSAGE));

        Account account = accountRepo.findById(accountId)
                .orElseThrow(() -> new ResourceNotFoundException(NO_ACCOUNT_MESSAGE));

        project.addAccount(account);
        projectRepo.save(project);
        return new ArrayList<>(projectRepo.getOne(projectId).getAccounts());
    }

    @Override
    public void deleteAccountFromProject(Long projectId, Long accountId) {
        Project project = projectRepo.findById(projectId)
                .orElseThrow(() -> new ResourceNotFoundException(NO_PROJECT_MESSAGE));

        Account account = accountRepo.findById(accountId)
                .orElseThrow(() -> new ResourceNotFoundException(NO_ACCOUNT_MESSAGE));

        if (!project.getAccounts().contains(account)) {
            throw new ResourceNotFoundException("There is already no account with this ID in this project");
        }
        project.deleteAccount(account);
        projectRepo.save(project);
    }
}
