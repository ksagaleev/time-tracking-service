package com.ksagaleev.timetrackingservice.service.impl;

import com.ksagaleev.timetrackingservice.model.entity.TimeTrackRecord;
import com.ksagaleev.timetrackingservice.repo.TimeTrackRecordRepo;
import com.ksagaleev.timetrackingservice.service.TimeTrackRecordService;
import com.ksagaleev.timetrackingservice.validation.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class TimeTrackRecordServiceImpl implements TimeTrackRecordService {

    @Autowired
    private TimeTrackRecordRepo timeTrackRecordRepo;
    private static final Logger logger = LoggerFactory.getLogger(TimeTrackRecordServiceImpl.class);
    private final String NO_RECORD_MESSAGE = "TimeTrackRecord with this ID does not exist";

    @Override
    public List<TimeTrackRecord> getAll() {
        return timeTrackRecordRepo.findAll();
    }

    @Override
    public TimeTrackRecord getTimeTrackRecord(Long id) {
        return timeTrackRecordRepo.findById(id).<ResourceNotFoundException>orElseThrow(() -> {
            throw new ResourceNotFoundException(NO_RECORD_MESSAGE);
        });
    }

    @Override
    public TimeTrackRecord addTimeTrackRecord(TimeTrackRecord timeTrackRecordToSave) {
        return timeTrackRecordRepo.save(timeTrackRecordToSave);
    }

    @Override
    public TimeTrackRecord updateTimeTrackRecord(Long id, TimeTrackRecord timeTrackRecordToUpdate) {
        return timeTrackRecordRepo.findById(id).map(timeTrackRecord -> {
            timeTrackRecord.setTimeSpent(timeTrackRecordToUpdate.getTimeSpent());
            timeTrackRecord.setDate(timeTrackRecordToUpdate.getDate());
            timeTrackRecord.setAccount(timeTrackRecordToUpdate.getAccount());
            timeTrackRecord.setComment(timeTrackRecordToUpdate.getComment());
            timeTrackRecord.setProject(timeTrackRecordToUpdate.getProject());
            return timeTrackRecordRepo.save(timeTrackRecord);
        }).orElseThrow(() -> new ResourceNotFoundException(NO_RECORD_MESSAGE));
    }

    @Override
    public void deleteTimeTrackRecord(Long id) {
        TimeTrackRecord recordToDelete = timeTrackRecordRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(NO_RECORD_MESSAGE));
        timeTrackRecordRepo.delete(recordToDelete);
    }

    @Override
    public void finalizeRecordsOlderThan(LocalDate lockDate) {
        List<TimeTrackRecord> nonFinalizedRecords = timeTrackRecordRepo.findAllByIsFinalized(false);
        nonFinalizedRecords.forEach(record -> {
            if (record.getDate().isBefore(lockDate)) {
                record.setIsFinalized(true);
                timeTrackRecordRepo.save(record);
            }
        });
    }
}
