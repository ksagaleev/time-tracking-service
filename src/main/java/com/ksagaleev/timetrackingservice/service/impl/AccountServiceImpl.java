package com.ksagaleev.timetrackingservice.service.impl;

import com.ksagaleev.timetrackingservice.model.dto.AddAccountDTO;
import com.ksagaleev.timetrackingservice.model.entity.*;
import com.ksagaleev.timetrackingservice.repo.AccountRepo;
import com.ksagaleev.timetrackingservice.repo.PermissionRepo;
import com.ksagaleev.timetrackingservice.repo.TimeTrackRecordRepo;
import com.ksagaleev.timetrackingservice.service.AccountService;
import com.ksagaleev.timetrackingservice.validation.IllegalDtoParameterException;
import com.ksagaleev.timetrackingservice.validation.NoAccessToProjectException;
import com.ksagaleev.timetrackingservice.validation.ResourceNotFoundException;
import com.ksagaleev.timetrackingservice.validation.WrongAccountException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.ksagaleev.timetrackingservice.security.PermissionsConfig.TRACK_TIME;
import static org.springframework.ldap.query.LdapQueryBuilder.query;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private PermissionRepo permissionRepo;

    @Autowired
    private TimeTrackRecordRepo timeTrackRecordRepo;

    @Autowired
    private LdapTemplate ldapTemplate;

    @Autowired
    private static Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    private final String NO_ACCOUNT_MESSAGE = "User with this ID does not exist";

    @Override
    public List<Account> getAll() {
        return accountRepo.findAll();
    }

    @Override
    public Account getAccount(Long id) {
        return accountRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(NO_ACCOUNT_MESSAGE));
    }

    @Override
    public Account addAccount(AddAccountDTO addAccountDTO) {
        if (accountRepo.findAccountByEmail(addAccountDTO.getEmail()) != null) {
            throw new IllegalDtoParameterException("Account with this email exist");
        } else if (accountRepo.findAccountByUsername(addAccountDTO.getLdapUsername()) != null) {
            throw new IllegalDtoParameterException("Account with this ldapUsername exist");
        }

        Account accountToAdd = new Account();
        accountToAdd.setUsername(addAccountDTO.getLdapUsername());
        accountToAdd.setPassword(addAccountDTO.getLdapPassword());
        accountToAdd.setEmail(addAccountDTO.getEmail());
        accountToAdd.setName(addAccountDTO.getName());
        accountToAdd.setSurname(addAccountDTO.getSurname());
        accountToAdd.addAuthority(permissionRepo.findPermissionByAuthority(TRACK_TIME));

        LdapPerson ldapPerson = new LdapPerson(
                addAccountDTO.getLdapUsername(),
                addAccountDTO.getName()+ " " + addAccountDTO.getSurname(),
                addAccountDTO.getSurname(),
                "people",
                addAccountDTO.getLdapPassword()
        );

        ldapTemplate.create(ldapPerson);
        return accountRepo.save(accountToAdd);
    }

    @Override
    public boolean deleteAccount(Long id) {
        Account account = accountRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(NO_ACCOUNT_MESSAGE));
        LdapPerson ldapPerson = ldapTemplate.findOne(query().where("uid").is(account.getUsername()), LdapPerson.class);
        logger.debug("deleting account from db and ldap:" + ldapPerson);
        ldapTemplate.delete(ldapPerson);
        account.setAuthorities(new HashSet<>());
        accountRepo.save(account);
        accountRepo.delete(account);
        return true;
    }

    @Override
    public Account updateAccount(Long id, Account accountToUpdate) {
        return accountRepo.findById(id)
                .map(account -> {
                    account.setName(accountToUpdate.getName());
                    account.setSurname(accountToUpdate.getSurname());
                    account.setEmail(accountToUpdate.getEmail());
                    return accountRepo.save(account);
                })
                .orElseThrow(() -> new ResourceNotFoundException("User with this ID does not exist"));
    }

    @Override
    public List<TimeTrackRecord> getAccountTimeTrackRecords(Long id) {
        Account account = accountRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(NO_ACCOUNT_MESSAGE));
        return account.getTimeTrackRecords();
    }

    @Override
    public List<TimeTrackRecord> addTimeTrackRecordToAccount(Long id, TimeTrackRecord timeTrackRecord) {
        Account account = accountRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(NO_ACCOUNT_MESSAGE));
        account.addTimeTrackRecord(timeTrackRecord);
        accountRepo.save(account);
        return accountRepo.getOne(id).getTimeTrackRecords();
    }

    @Override
    public Set<Permission> getAccountPermissions(Long id) {
        Account account = accountRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(NO_ACCOUNT_MESSAGE));
        return account.getAuthorities();
    }

    @Override
    public Set<Permission> addPermissionToAccount(Long id, Permission permissionToAdd) {
        Account account = accountRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(NO_ACCOUNT_MESSAGE));

        Permission permissionInRepo = permissionRepo.findPermissionByAuthority(permissionToAdd.getAuthority());
        if (permissionInRepo == null) {
            throw new ResourceNotFoundException("Permission with this authority does not exist");
        }

        account.addAuthority(permissionInRepo);
        accountRepo.save(account);
        return accountRepo.getOne(id).getAuthorities();
    }

    @Override
    public Account getMe(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        Account account = accountRepo.findAccountByUsername(userDetails.getUsername());
        if (account == null) throw new ResourceNotFoundException("Your user is present in LDAP, but" +
                " not present in database. Contact your manager with this issue.");
        return account;
    }

    @Override
    public Set<Project> getMyAvailableProjects(Authentication authentication) {
        Account myAccount = getMe(authentication);
        return myAccount.getProjects();
    }

    @Override
    public List<TimeTrackRecord> getMyTimeTrackRecords(Authentication authentication) {
        Account myAccount = getMe(authentication);
        return myAccount.getTimeTrackRecords();
    }


    @Override
    public List<TimeTrackRecord> trackMyTime(Authentication authentication, TimeTrackRecord record) {
        Account myAccount = getMe(authentication);

        if (!record.getAccount().equals(myAccount)) {
            throw new WrongAccountException("You are trying to track time for another user. Please input your account uid!");
        }

        checkAccessToProject(myAccount, record);
        myAccount.addTimeTrackRecord(record);
        accountRepo.save(myAccount);
        return accountRepo.getOne(myAccount.getId()).getTimeTrackRecords();
    }

    @Override
    public TimeTrackRecord updateMyTimeTrackRecord(Authentication authentication, Long recordId, TimeTrackRecord recordToUpdate) {
        TimeTrackRecord recordInDb = timeTrackRecordRepo.findById(recordId)
                .orElseThrow(() -> new ResourceNotFoundException("TimeTrackRecord with this ID does not exist"));

        Account myAccount = getMe(authentication);
        if (!myAccount.getTimeTrackRecords().contains(recordInDb)) {
            throw new WrongAccountException("You are trying to update not yours time-track-record. You can update " +
                    "only records that are mapped to your account.");
        }

        if (!recordToUpdate.getAccount().equals(myAccount)) {
            throw new WrongAccountException("You are trying to track time for another user. Please input your account uid!");
        }

        if (recordInDb.getIsFinalized()) {
            throw new IllegalDtoParameterException("Time-track-record is already finalized. Contact to support" +
                    " to update this time-track-record");
        }

        checkAccessToProject(myAccount, recordToUpdate);

        recordInDb.setProject(recordToUpdate.getProject());
        recordInDb.setComment(recordToUpdate.getComment());
        recordInDb.setDate(recordToUpdate.getDate());
        recordInDb.setTimeSpent(recordToUpdate.getTimeSpent());
        return timeTrackRecordRepo.save(recordInDb);
    }

    private void checkAccessToProject(Account account, TimeTrackRecord record) {
        Set<Project> availableProjects = account.getProjects();
        if (!availableProjects.contains(record.getProject())) {
            throw new NoAccessToProjectException("You don't have access to this project. " +
                    "Track time to the project you have access to(/api/accounts/me/projects)");
        }
    }
}
