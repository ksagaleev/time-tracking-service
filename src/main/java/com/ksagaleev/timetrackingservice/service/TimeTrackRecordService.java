package com.ksagaleev.timetrackingservice.service;

import com.ksagaleev.timetrackingservice.model.entity.TimeTrackRecord;

import java.time.LocalDate;
import java.util.List;

public interface TimeTrackRecordService {
    List<TimeTrackRecord> getAll();
    TimeTrackRecord getTimeTrackRecord(Long id);
    TimeTrackRecord addTimeTrackRecord(TimeTrackRecord timeTrackRecordToSave);
    TimeTrackRecord updateTimeTrackRecord(Long id, TimeTrackRecord timeTrackRecordToUpdate);
    void deleteTimeTrackRecord(Long id);
    void finalizeRecordsOlderThan(LocalDate lockDate);
}
