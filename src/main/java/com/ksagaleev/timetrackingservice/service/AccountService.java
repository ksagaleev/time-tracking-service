package com.ksagaleev.timetrackingservice.service;

import com.ksagaleev.timetrackingservice.model.dto.AddAccountDTO;
import com.ksagaleev.timetrackingservice.model.entity.Account;
import com.ksagaleev.timetrackingservice.model.entity.Permission;
import com.ksagaleev.timetrackingservice.model.entity.Project;
import com.ksagaleev.timetrackingservice.model.entity.TimeTrackRecord;
import org.springframework.security.core.Authentication;

import java.util.List;
import java.util.Set;

public interface AccountService {
    List<Account> getAll();
    Account getAccount(Long id);
    Account addAccount(AddAccountDTO addAccountDTO);
    boolean deleteAccount(Long id);
    Account updateAccount(Long id, Account accountToUpdate);
    List<TimeTrackRecord> getAccountTimeTrackRecords(Long id);
    List<TimeTrackRecord> addTimeTrackRecordToAccount(Long id, TimeTrackRecord timeTrackRecord);
    Set<Permission> getAccountPermissions(Long id);
    Set<Permission> addPermissionToAccount(Long id, Permission permissionToAdd);
    Account getMe(Authentication authentication);
    Set<Project> getMyAvailableProjects(Authentication authentication);
    List<TimeTrackRecord> getMyTimeTrackRecords(Authentication authentication);
    List<TimeTrackRecord> trackMyTime(Authentication authentication, TimeTrackRecord timeTrackRecord);
    TimeTrackRecord updateMyTimeTrackRecord(Authentication authentication, Long recordId, TimeTrackRecord recordToUpdate);
}
