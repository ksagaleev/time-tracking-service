package com.ksagaleev.timetrackingservice.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IllegalDtoParameterException extends RuntimeException {
    public IllegalDtoParameterException() {
    }

    public IllegalDtoParameterException(String message) {
        super(message);
    }

    public IllegalDtoParameterException(Throwable cause) {
        super(cause);
    }
}
