package com.ksagaleev.timetrackingservice.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NoAccessToProjectException extends RuntimeException {
    public NoAccessToProjectException() {
    }

    public NoAccessToProjectException(String message) {
        super(message);
    }
}
