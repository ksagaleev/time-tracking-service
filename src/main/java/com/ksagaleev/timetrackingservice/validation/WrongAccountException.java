package com.ksagaleev.timetrackingservice.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class WrongAccountException extends RuntimeException{
    public WrongAccountException() {
    }

    public WrongAccountException(String message) {
        super(message);
    }
}
