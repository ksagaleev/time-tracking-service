package com.ksagaleev.timetrackingservice.repo;

import com.ksagaleev.timetrackingservice.model.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepo extends JpaRepository<Project, Long> {
}
