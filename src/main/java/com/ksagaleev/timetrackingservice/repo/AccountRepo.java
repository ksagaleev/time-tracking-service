package com.ksagaleev.timetrackingservice.repo;

import com.ksagaleev.timetrackingservice.model.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepo extends JpaRepository<Account, Long> {
    Account findAccountByUsername(String username);
    Account findAccountByEmail(String email);
}
