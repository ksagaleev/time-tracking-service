package com.ksagaleev.timetrackingservice.repo;

import com.ksagaleev.timetrackingservice.model.entity.TimeTrackRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimeTrackRecordRepo extends JpaRepository<TimeTrackRecord, Long> {
    List<TimeTrackRecord> findAllByIsFinalized(Boolean isFinalized);
}
