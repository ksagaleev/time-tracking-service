package com.ksagaleev.timetrackingservice.repo;

import com.ksagaleev.timetrackingservice.model.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionRepo extends JpaRepository<Permission, Long> {
    Permission findPermissionByAuthority(String authority);
}
