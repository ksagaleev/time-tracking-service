package com.ksagaleev.timetrackingservice.scheduledTasks;

import com.ksagaleev.timetrackingservice.service.TimeTrackRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class TimeTrackRecordLocker {

    private static final Logger logger = LoggerFactory.getLogger(TimeTrackRecordLocker.class);

    @Autowired
    private TimeTrackRecordService timeTrackRecordService;

    @Scheduled(cron = "0 15 10 ? * MON")
    public void lockTimeTrackRecords() {
        timeTrackRecordService.finalizeRecordsOlderThan(LocalDate.now());
        logger.info("All time-track-records for previous week have been finalized");
    }
}
