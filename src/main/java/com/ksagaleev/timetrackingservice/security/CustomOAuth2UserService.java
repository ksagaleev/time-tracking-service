package com.ksagaleev.timetrackingservice.security;

import com.ksagaleev.timetrackingservice.model.entity.Account;
import com.ksagaleev.timetrackingservice.repo.AccountRepo;
import com.ksagaleev.timetrackingservice.validation.WrongAccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class CustomOAuth2UserService implements OAuth2UserService<OidcUserRequest, OidcUser> {

    @Autowired
    private AccountRepo accountRepo;

    @Override
    public OidcUser loadUser(OidcUserRequest userRequest) throws OAuth2AuthenticationException {
        final OidcUserService delegate = new OidcUserService();
        OidcUser oidcUser = delegate.loadUser(userRequest);

        String email = (String) oidcUser.getAttributes().get("email");
        Account accInDb = accountRepo.findAccountByEmail(email);

        if (accInDb == null) {
            throw new WrongAccountException("Authentication problem, your email is not presented in our system: " + email);
        }

        Set<GrantedAuthority> mappedAuthorities = new HashSet<>(accInDb.getAuthorities());
        return new DefaultOidcUser(mappedAuthorities, oidcUser.getIdToken(), oidcUser.getUserInfo());
    }
}
