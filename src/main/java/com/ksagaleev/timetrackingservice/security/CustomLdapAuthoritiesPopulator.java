package com.ksagaleev.timetrackingservice.security;

import com.ksagaleev.timetrackingservice.model.entity.Account;
import com.ksagaleev.timetrackingservice.model.entity.Permission;
import com.ksagaleev.timetrackingservice.repo.AccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Component
public class CustomLdapAuthoritiesPopulator implements LdapAuthoritiesPopulator {

    @Autowired
    private AccountRepo accountRepo;

    @Override
    public Collection<Permission> getGrantedAuthorities(DirContextOperations userData, String username) {
        Set<Permission> authorities = new HashSet<>();
        Account account = accountRepo.findAccountByUsername(username);

        if (account != null) {
            authorities = account.getAuthorities();
        }

        return authorities;
    }
}
