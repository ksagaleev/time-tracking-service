package com.ksagaleev.timetrackingservice.security;

public class PermissionsConfig {
    public static final String CHANGE_ACCOUNTS ="ROLE_CHANGE_ACCOUNTS";
    public static final String CHANGE_ACCESS ="ROLE_CHANGE_ACCESS";
    public static final String CHANGE_RECORDS ="ROLE_CHANGE_RECORDS";
    public static final String MANAGE_PROJECTS ="ROLE_MANAGE_PROJECTS";
    public static final String TRACK_TIME ="ROLE_TRACK_TIME";
    public static final String LOCK_RECORDS ="ROLE_LOCK_RECORDS";
    public static final String REPORT ="ROLE_REPORT";
}
