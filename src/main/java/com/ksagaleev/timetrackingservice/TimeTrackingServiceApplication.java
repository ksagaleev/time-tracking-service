package com.ksagaleev.timetrackingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TimeTrackingServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(TimeTrackingServiceApplication.class, args);
    }
}
