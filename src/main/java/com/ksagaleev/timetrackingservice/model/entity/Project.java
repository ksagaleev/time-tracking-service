package com.ksagaleev.timetrackingservice.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Table(name = "projects")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Project parentProject;

    @OneToMany(mappedBy="parentProject")
    private List<Project> childProjects;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    private List<TimeTrackRecord> timeTrackRecords;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "project_accounts",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "account_id")
    )
    private Set<Account> accounts;

    public void addAccount(Account account) {
        this.accounts.add(account);
    }

    public void deleteAccount(Account account) {
        this.accounts.remove(account);
    }
}
