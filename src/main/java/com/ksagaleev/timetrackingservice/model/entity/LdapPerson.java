package com.ksagaleev.timetrackingservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.ldap.odm.annotations.*;
import org.springframework.ldap.support.LdapNameBuilder;

import javax.naming.Name;

@Entry(
        objectClasses = {"inetOrgPerson", "organizationalPerson", "person", "top"}
)
@Data
@AllArgsConstructor
@NoArgsConstructor
public final class LdapPerson{

    @Id
    private Name dn;

    @DnAttribute(value = "uid", index = 1)
    private String uid;

    @DnAttribute(value = "ou", index = 0)
    @Transient
    private String group;

    @Attribute(name = "cn")
    private String fullName;

    @Attribute(name = "sn")
    private String lastName;

    @Attribute(name = "userPassword")
    private String password;

    public LdapPerson(String uid, String fullName, String lastName, String group, String password) {
        this.dn = LdapNameBuilder.newInstance("uid=" + uid + ",ou=" + group).build();
        this.uid = uid;
        this.fullName = fullName;
        this.lastName = lastName;
        this.group = group;
        this.password = password;
    }
}
