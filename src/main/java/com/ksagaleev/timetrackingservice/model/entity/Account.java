package com.ksagaleev.timetrackingservice.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity
@Data
@ToString(exclude = {"timeTrackRecords", "projects"})
@EqualsAndHashCode(exclude = {"timeTrackRecords", "projects"})
@Table(name = "accounts")
public class Account implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(nullable = false)
    private String username;

    @Column(columnDefinition = "varchar default null")
    private String password;

    @Column(columnDefinition = "boolean default false")
    private boolean isAccountNonExpired;

    @Column(columnDefinition = "boolean default false")
    private boolean isAccountNonLocked;

    @Column(columnDefinition = "boolean default false")
    private boolean isCredentialsNonExpired;

    @Column(columnDefinition = "boolean default false")
    private boolean isEnabled;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @Column(nullable = false, unique = true)
    private String email;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private List<TimeTrackRecord> timeTrackRecords = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "account_permissions",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id"))
    private Set<Permission> authorities = new HashSet<>();

    @ManyToMany(mappedBy = "accounts")
    private Set<Project> projects;

    public void addTimeTrackRecord(TimeTrackRecord timeTrackRecord) {
        this.timeTrackRecords.add(timeTrackRecord);
    }

    public void addAuthority(Permission permission) {
        this.authorities.add(permission);
    }
}
