package com.ksagaleev.timetrackingservice.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@Table(name = "time_track_records")
public class TimeTrackRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "time_spent", nullable = false)
    private Integer timeSpent;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @ManyToOne
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @Column(nullable = false)
    private LocalDate date;

    @Column(name = "is_finalized", nullable = false, columnDefinition = "boolean default false")
    private Boolean isFinalized;

    private String comment;
}
