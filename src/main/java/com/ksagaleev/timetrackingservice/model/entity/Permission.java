package com.ksagaleev.timetrackingservice.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Data
@Table(name = "permissions")
@ToString(exclude = "accounts")
@EqualsAndHashCode(exclude = {"id", "accounts"})
public class Permission implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(nullable = false)
    @NotBlank
    private String authority;

    @JsonIgnore
    @ManyToMany(mappedBy = "authorities")
    private Set<Account> accounts;
}
