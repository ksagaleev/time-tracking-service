package com.ksagaleev.timetrackingservice.model.converter;

import java.util.List;

public interface DtoConverter<D,E> {
    D convertToDTO(E entity);
    E convertToEntity(D dto);
    List<D> convertAllToDTO(List<E> entities);
    List<E> convertAllToEntity(List<D> dtos);
}
