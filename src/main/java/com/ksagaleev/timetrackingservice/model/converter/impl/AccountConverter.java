package com.ksagaleev.timetrackingservice.model.converter.impl;

import com.ksagaleev.timetrackingservice.model.converter.DtoConverter;
import com.ksagaleev.timetrackingservice.model.dto.AccountDTO;
import com.ksagaleev.timetrackingservice.model.entity.Account;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AccountConverter implements DtoConverter<AccountDTO, Account> {

    @Override
    public AccountDTO convertToDTO(Account entity) {
        if(entity == null) return null;

        AccountDTO dto = new AccountDTO();
        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setName(entity.getName());
        dto.setSurname(entity.getSurname());
        dto.setLdapUsername(entity.getUsername());

        return dto;
    }

    @Override
    public Account convertToEntity(AccountDTO dto) {
        if (dto == null) return null;

        Account entity = new Account();
        entity.setId(dto.getId());
        entity.setEmail(dto.getEmail());
        entity.setName(dto.getName());
        entity.setSurname(dto.getSurname());
        return entity;
    }

    @Override
    public List<AccountDTO> convertAllToDTO(List<Account> entities) {
        return entities.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    @Override
    public List<Account> convertAllToEntity(List<AccountDTO> dtos) {
        return dtos.stream().map(this::convertToEntity).collect(Collectors.toList());
    }
}
