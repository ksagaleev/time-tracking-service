package com.ksagaleev.timetrackingservice.model.converter.impl;

import com.ksagaleev.timetrackingservice.model.converter.DtoConverter;
import com.ksagaleev.timetrackingservice.model.dto.TimeTrackRecordDTO;
import com.ksagaleev.timetrackingservice.model.entity.TimeTrackRecord;
import com.ksagaleev.timetrackingservice.repo.AccountRepo;
import com.ksagaleev.timetrackingservice.repo.ProjectRepo;
import com.ksagaleev.timetrackingservice.validation.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TimeTrackRecordConverter implements DtoConverter<TimeTrackRecordDTO, TimeTrackRecord> {

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private ProjectRepo projectRepo;

    @Override
    public TimeTrackRecordDTO convertToDTO(TimeTrackRecord entity) {
        if (entity == null) return null;

        TimeTrackRecordDTO dto = new TimeTrackRecordDTO();
        dto.setId(entity.getId());
        dto.setAccountId(entity.getAccount().getId());
        dto.setProjectId(entity.getProject().getId());
        dto.setTimeSpent(entity.getTimeSpent());
        dto.setDate(entity.getDate());
        dto.setIsFinalized(entity.getIsFinalized());
        dto.setComment(entity.getComment());
        return dto;
    }

    @Override
    public TimeTrackRecord convertToEntity(TimeTrackRecordDTO dto) {
        if (dto == null) return null;

        TimeTrackRecord entity = new TimeTrackRecord();
        entity.setId(dto.getId());
        entity.setAccount(accountRepo.findById(dto.getAccountId())
                .orElseThrow(() -> new ResourceNotFoundException("Account with this ID does not exist")));
        entity.setProject(projectRepo.findById(dto.getProjectId())
                .orElseThrow(() -> new ResourceNotFoundException("Project with this ID does not exist")));
        entity.setTimeSpent(dto.getTimeSpent());
        entity.setDate(dto.getDate());
        entity.setComment(dto.getComment());
        return entity;
    }

    @Override
    public List<TimeTrackRecordDTO> convertAllToDTO(List<TimeTrackRecord> entities) {
        if (entities == null) return null;
        return entities.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    @Override
    public List<TimeTrackRecord> convertAllToEntity(List<TimeTrackRecordDTO> dtos) {
        if (dtos == null) return null;
        return dtos.stream().map(this::convertToEntity).collect(Collectors.toList());
    }

}
