package com.ksagaleev.timetrackingservice.model.converter.impl;

import com.ksagaleev.timetrackingservice.model.converter.DtoConverter;
import com.ksagaleev.timetrackingservice.model.dto.ProjectDTO;
import com.ksagaleev.timetrackingservice.model.entity.Project;
import com.ksagaleev.timetrackingservice.repo.ProjectRepo;
import com.ksagaleev.timetrackingservice.validation.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ProjectConverter implements DtoConverter<ProjectDTO, Project> {

    @Autowired
    private ProjectRepo projectRepo;

    @Override
    public ProjectDTO convertToDTO(Project entity) {
        if (entity == null) return null;

        ProjectDTO dto = new ProjectDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setParentProjectId(Optional.ofNullable(entity.getParentProject())
                .map(Project::getId)
                .orElse(null));
        return dto;
    }

    @Override
    public Project convertToEntity(ProjectDTO dto) {
        if (dto == null) return null;

        Project entity = new Project();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setParentProject(projectRepo.findById(dto.getParentProjectId())
                .orElseThrow(() -> new ResourceNotFoundException("Project with this ID can not be parent cause it does not exist")));
        return entity;
    }

    @Override
    public List<ProjectDTO> convertAllToDTO(List<Project> entities) {
        return entities.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    @Override
    public List<Project> convertAllToEntity(List<ProjectDTO> dtos) {
        return dtos.stream().map(this::convertToEntity).collect(Collectors.toList());
    }
}
