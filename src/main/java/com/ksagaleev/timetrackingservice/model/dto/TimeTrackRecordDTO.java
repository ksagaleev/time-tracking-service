package com.ksagaleev.timetrackingservice.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
public class TimeTrackRecordDTO implements Serializable {

    private Long id;

    @NotNull(message = "timeSpent can not be blank")
    private Integer timeSpent;

    @NotNull(message = "account_id can not be blank")
    private Long accountId;

    @NotNull(message = "project_id can not be blank")
    private Long projectId;

    @NotNull(message = "date can not be blank")
    private LocalDate date;

    private Boolean isFinalized;

    private String comment;
}
