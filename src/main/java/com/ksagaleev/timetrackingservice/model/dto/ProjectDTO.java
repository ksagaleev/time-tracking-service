package com.ksagaleev.timetrackingservice.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
public class ProjectDTO implements Serializable {

    private Long id;

    @NotBlank(message = "name can not be blank")
    private String name;

    private Long parentProjectId;
}
