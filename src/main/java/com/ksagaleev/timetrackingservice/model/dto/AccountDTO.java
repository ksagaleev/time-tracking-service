package com.ksagaleev.timetrackingservice.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
public class AccountDTO implements Serializable {

    private Long id;

    @NotBlank(message = "name can not be blank")
    private String name;

    @NotBlank(message = "lastname can not be blank")
    private String surname;

    @NotBlank(message = "ldapUsername can not be blank")
    private String ldapUsername;

    @Email
    @NotBlank(message = "email can not be blank")
    private String email;
}
